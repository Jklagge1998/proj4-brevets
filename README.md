# Project 4: Brevet time calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

Author: Jackson Klagge (Jklagge1998@gmail.com)

About the Project: My task was to implement the Brevet Calculator using ajax, flask, and python.

Assumptions made: I decided to build a table based on the one given to us. I decided to cut control points right before the given amount. For example 200 will be calculated in the second column while 199 will be in the first. There is no overlap. Secondly I assumed that the user input will be in the range of 0 - 1300.

The source code for this project can be found at bitbucket.org/UOCIS322/proj4-brevets.
