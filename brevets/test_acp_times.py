import acp_times
import nose
import arrow

date = "2017-01-01T00:00:00+00:00"

def test_edges():
    """This function tests acp_times.open_time and acp_times.close_time
       for the input 0."""
    assert acp_times.open_time(0, 200, date) == arrow.get(date).shift(hours=+0, minutes=+0).isoformat()
    assert acp_times.close_time(0, 200, date) == arrow.get(date).shift(hours=+0, minutes=+0).isoformat()

def test_boundary_1():
    """This function tests acp_times.open_time and acp_times.close_time and the
       boundary of column 0 and 1. (199 is in col 0 and 200 is in col 1 )
       """
    assert acp_times.open_time(199, 200, date) == arrow.get(date).shift(hours=+5, minutes=+51).isoformat()
    assert acp_times.close_time(199, 200, date) == arrow.get(date).shift(hours=+13, minutes=+16).isoformat()
    assert acp_times.open_time(200, 200, date) == arrow.get(date).shift(hours=+6, minutes=+15).isoformat()
    assert acp_times.close_time(200, 200, date) == arrow.get(date).shift(hours=+13, minutes=+20).isoformat()

def test_boundary_2():
	"""This function tests acp_times.open_time and acp_times.close_time and the
       boundary of column 1 and 2. (399 is in col 1 and 400 is in col 2 )
       """
    assert acp_times.open_time(399, 400, date) == arrow.get(date).shift(hours=+12, minutes=+28).isoformat()
    assert acp_times.close_time(399, 400, date) == arrow.get(date).shift(hours=+26, minutes=+36).isoformat()
    assert acp_times.open_time(400, 200, date) == arrow.get(date).shift(hours=+13, minutes=+20).isoformat()
    assert acp_times.close_time(400, 200, date) == arrow.get(date).shift(hours=+26, minutes=+40).isoformat()

def test_boundary_3():
	"""This function tests acp_times.open_time and acp_times.close_time and the
       boundary of column 2 and 3. (599 is in col 2 and  600 is in col 3)
       """
    assert acp_times.open_time(599, 200, date) == arrow.get(date).shift(hours=+19, minutes=+58).isoformat()
    assert acp_times.close_time(599, 200, date) == arrow.get(date).shift(hours=+39, minutes=+56).isoformat()
    assert acp_times.open_time(600, 200, date) == arrow.get(date).shift(hours=+21, minutes=+35).isoformat()
    assert acp_times.close_time(600, 200, date) == arrow.get(date).shift(hours=+52, minutes=+30).isoformat()

def test_boundary_4():
	"""This function tests acp_times.open_time and acp_times.close_time and the
       boundary of column 3 and 4. (999 is in col 3 and 1000 is in col 4 )
       """
    assert acp_times.open_time(999, 200, date) == arrow.get(date).shift(hours=+35, minutes=+40).isoformat()
    assert acp_times.close_time(999, 200, date) == arrow.get(date).shift(hours=+87, minutes=+25).isoformat()
    assert acp_times.open_time(1000, 200, date) == arrow.get(date).shift(hours=+38, minutes=+27).isoformat()
    assert acp_times.close_time(1000, 200, date) == arrow.get(date).shift(hours=+75, minutes=+0).isoformat()     
