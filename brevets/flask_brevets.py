"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.info("km={}".format(km))
    app.logger.info("request.args: {}".format(request.args))
    brevet_dist_km = request.args.get('brevet_dist_km', type=str)
    app.logger.info("brevet_dist_km: {}".format(brevet_dist_km))
    begin_date = request.args.get('begin_date', type=str)
    app.logger.info("begin_date: {}".format(begin_date))
    begin_time = request.args.get('begin_time', type=str)
    app.logger.info("begin_time: {}".format(begin_time))
    start_time = "{} {}".format(begin_date, begin_time)
    app.logger.info("start_time before arrow: {}".format(start_time))
    start_time = arrow.get(start_time)
    app.logger.info("start_time after arrow: {}".format(start_time))

    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, brevet_dist_km, start_time)
    close_time = acp_times.close_time(km, brevet_dist_km, start_time)
    app.logger.info("open_time: {}".format(open_time))
    app.logger.info("close_time: {}".format(close_time))
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
